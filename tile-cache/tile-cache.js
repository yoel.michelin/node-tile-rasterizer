const express = require('express');
const axios = require('axios');
const cors = require("cors");
const Redis = require('ioredis');
const { createProxyMiddleware } = require('http-proxy-middleware');

const app = express();

const corsOptions = {
	origin: "*"
};

app.use(cors(corsOptions));

// Initialize the Redis client
const client = new Redis({
	host: 'redis',
	port: 6379
});

const CACHE_TTL = 3600; // Time to live for cache in seconds
const TARGET_API = 'http://tile-converter:3001'; // Placeholder API endpoint

async function cacheMiddleware(req, res, next) {
	const key = `cache:${req.originalUrl}`;

	try {
		// Attempt to retrieve the content type and response data from Redis
		let contentType;// = await client.get(`${key}:type`);
		let responseData = await client.getBuffer(`${key}`);

		if (responseData) {
			contentType = `image/${key.slice(-3)}`;
			console.log('Cache hit', contentType);
			res.setHeader('Content-Type', contentType);
			return res.end(responseData, 'binary');
		}

		console.log('Cache miss:', `${key}`);
		const response = await axios.get(`${TARGET_API}${req.originalUrl}`, {
			responseType: 'arraybuffer' // Tell axios to handle the response as binary data
		});

		contentType = response.headers['content-type'];

		// Store the body and content type in the cache with expiration
		// await client.setex(key, CACHE_TTL, response.data); // Store the binary data
		// await client.setex(`${key}:type`, CACHE_TTL, contentType); // Store the content type

		res.setHeader('Content-Type', contentType);
		res.end(response.data, 'binary');
	} catch (error) {
		console.error('Error in cacheMiddleware:', error.message);
		res.status(500).json({ error: 'Internal Server Error' });
	}
}

// Apply the caching middleware and proxy middleware
app.use(cacheMiddleware);
// app.use(cacheMiddleware, createProxyMiddleware({
// 	target: TARGET_API,
// 	changeOrigin: true,
// 	pathRewrite: (path, req) => path
// }));

app.get("*", (req, res) => {
	console.log("REQUEST:", req.headers);
	console.log("url:",req.url);
});

const PORT = 3002;
app.listen(PORT, () => {
	console.log(`Server is running on port ${PORT}`);
});
