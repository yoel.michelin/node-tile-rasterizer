import axios from 'axios';
import sharp from 'sharp';

export async function editPixelsAndReturnBuffer(inputBuffer, color, lineThickness) {
	// Convert the input ArrayBuffer to a Buffer for processing with sharp
	const inputNodeBuffer = Buffer.from(inputBuffer);

	// Process the image with sharp
	const { data, info } = await sharp(inputNodeBuffer)
		.ensureAlpha()
		.raw()
		.toBuffer({ resolveWithObject: true });

	const { width, height, channels } = info;
	// console.log(width, height, channels);

	// Modify the pixel data
	// let lineThickness = 4;

	for (let i = 0; i < data.length; i += channels) {
		// Draw a border around the image

		// Draw left and right borders
		if (i % (width * channels) > (width * channels) - lineThickness * channels - channels * 2 || i % (width * channels) < lineThickness * channels) {
			// console.log("i: ", i, "i % (width * channels): ", i % (width * channels));
			data[i] = color[0];    // Red channel
			data[i + 1] = color[1]; // Green channel
			data[i + 2] = color[2]; // Blue channel
		}

		// Draw top and bottom borders
		if (Math.floor(i / (width * channels)) < lineThickness || Math.floor(i / (width * channels)) > height - lineThickness) {
			data[i] = color[0];    // Red channel
			data[i + 1] = color[1]; // Green channel
			data[i + 2] = color[2]; // Blue channel
		}
		// if (data[i] === 255 && data[i + 1] === 255 && data[i + 2] === 255) {
		// 	// Set pixels to black (change as needed)
		// 	data[i] = 0;    // Red channel
		// 	data[i + 1] = 0; // Green channel
		// 	data[i + 2] = 0; // Blue channel
		// 	// Alpha channel remains unchanged
		// }
	}

	// Convert the modified raw data back to a PNG ArrayBuffer
	const outputBuffer = await sharp(data, { raw: { width, height, channels } })
		.toFormat('png')
		.toBuffer();

	return outputBuffer; // Convert Node.js Buffer to ArrayBuffer
}

export function cropImage(imageBuffer, leftOffset, topOffset, width, height) {
	// console.log("left:", leftOffset, "top:", topOffset, "width:", width, "height:", height);
	return sharp(imageBuffer)
		.extract({ left: leftOffset, top: topOffset, width: width, height: height })
		.toBuffer();
}

export async function fetchImage(url) {
	const response = await axios({
		url,
		responseType: 'arraybuffer'
	});

	return Buffer.from(response.data);
}