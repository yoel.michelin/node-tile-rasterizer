// conversion functions from openstreetmap wiki page:
export function tile2long(x,z) {
	// console.log(x, z);
	return (x/Math.pow(2,z)*360-180);
}

export function tile2lat(y,z) {
	// console.log(y, z);
	var n = Math.PI-2*Math.PI*y/Math.pow(2,z);
	return (180/Math.PI*Math.atan(0.5*(Math.exp(n)-Math.exp(-n))));
}

export function tile2coord(z, x, y, mapDimensions) {
	const n = Math.pow(2, z);
	const lon_deg = x / n * 360.0 - 180.0;
	const lat_rad = Math.atan(Math.sinh(Math.PI * (1 - 2 * y / n)));
	const lat_deg = lat_rad * 180.0 / Math.PI;
	// console.log("lon:", lon_deg, "lat:", lat_deg, "z:", z, "x:", x, "y:", y);
	return { lon_deg, lat_deg };
}

export function determineParameters(size, z, x, y) {
	const { lon_deg, lat_deg } = tile2coord(z, x+0.5, y+0.5);
	return { lon: lon_deg, lat: lat_deg, zoom: z, width: size, height: size };
	// return { lon: tile2long(x+0.5,z), lat: tile2lat(y+0.5,z), zoom: z, width: size, height: size };
}

export function getMetaTileCoords(tileX, tileY, extra, mapDimensions) {
	let metaX = tileX;
	const gridSize = 2 * extra + 1;
	if (tileX % gridSize <= parseInt(gridSize / 2))
		metaX = tileX - (tileX % gridSize);
	else
		metaX = tileX + (gridSize - tileX % gridSize);

	let metaY = tileY;
	if (tileY % gridSize <= parseInt(gridSize / 2))
		metaY = tileY - (tileY % gridSize);
	else
		metaY = tileY + (gridSize - tileY % gridSize);

	if (metaY + extra > mapDimensions - 1)
		metaY -= (metaY + extra - (mapDimensions - 1));
	if (metaY - extra < 0)
		metaY += extra - metaY;

	return { metaX, metaY };
}

async function subscribeWaitReturnTile(channel, tileCacheKey) {
	await subClient.subscribe(channel);

	subClient.on('message', async (channel, message) => {
		if (message === 'READY') {
			console.log("Metatile ready, getting tile from cache...");
			const tileImage = await client.getBuffer(tileCacheKey);
			await subClient.unsubscribe(channel);
			console.log("Unsubscribed");

			if (!tileImage) {
				console.log("\nBIG ERROR" + `${tileCacheKey}\n`);
				res.status(500).send('Image not found in cache');
				// throw new Error('\'READY\' but no image in cache!');
			} else {
				console.log(`BIG SUCCESS ${tileCacheKey}`);
				res.set('Content-Type', `image/${format}`);
				res.send(tileImage);
			}
			return ;
		}
	});
}