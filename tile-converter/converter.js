import express from 'express';
import Redis from 'ioredis';
import cors from 'cors';
import fs from 'fs';

// Create an instance of express
const app = express();
const port = 3001;

// local utils
import { determineParameters, getMetaTileCoords } from './geoUtils.js';
import { editPixelsAndReturnBuffer, cropImage, fetchImage } from './imageUtils.js';

const corsOptions = {
	origin: "*"
};

app.use(cors(corsOptions));

// Initialize the Redis client
const client = new Redis({ host: 'redis', port: 6379 });
// initialize sub/pub redis
const subClient = new Redis({ host: 'redis', port: 6379 });

const CACHE_TTL = 3600; // cache time-to-live in seconds

app.get('/styles/:id/:tilesize/:extra/:z/:x/:y.:format', async (req, res) => {
	console.log("url:", req.originalUrl);
	let { id, tilesize, extra, z, x: origTileX, y: origTileY, format } = req.params;
	tilesize = parseInt(tilesize);
	extra = parseInt(extra);
	z = parseInt(z);
	origTileX = parseFloat(origTileX);
	origTileY = parseFloat(origTileY);
	const mapDimensions = Math.pow(2, z);
	const gridSize = 2 * extra + 1; // number of tiles in each direction
	const size = gridSize * tilesize; // size of the full image (in pixels)

	async function saveToCache(key, imageData) {
		// imageData = await editPixelsAndReturnBuffer(imageData, [0, 0, 255], 4);
		try {
			// console.log("KEY:", key);
			await client.setex(key, CACHE_TTL, imageData); // Store the binary data

		} catch (error) {
			console.error('Error sending image to cache:', error.message);
		}
	}

	function getCacheKey(type, tileX, tileY) {
		return(`${type}:/styles/${id}/${tilesize}/${extra}/${z}/${tileX}/${tileY}.${format}`);
	}

	async function fetchMetaTile(url, metaX, metaY, gridSize, x, y) {
		let tileImage;

		let fullImage = await fetchImage(url);
		// fullImage = await editPixelsAndReturnBuffer(fullImage, [0, 255, 0], 8);

		for (let i = 0; i < gridSize; i++) {
			for (let j = 0; j < gridSize; j++) {
				let croppedBuffer = await cropImage(fullImage, i * tilesize, j * tilesize, tilesize, tilesize);
	
				const { tileX, tileY } = { 
					tileX: (metaX - extra + i) % mapDimensions, 
					tileY: (metaY - extra + j) % mapDimensions,
				};

				if (tileX == x && tileY == y) { tileImage = croppedBuffer };
				await saveToCache(getCacheKey('cache', tileX, tileY), croppedBuffer);
			}
		}
		return tileImage;
	}

	try {
		const tileCacheKey = getCacheKey('cache', origTileX, origTileY);
		if (z < 3) { // return normal tiles for zoom < 3
			let tileUrl = `http://tileserver-gl:8080/styles/${id}/${z}/${origTileX}/${origTileY}.${format}`;
			const tileImage = await fetchImage(tileUrl);
			await saveToCache(tileCacheKey, tileImage);
			res.set('Content-Type', `image/${format}`);
			res.send(tileImage);
			return ;
		} else {
			const { metaX, metaY } = getMetaTileCoords(origTileX, origTileY, extra, mapDimensions);
			const { lon, lat, zoom, width, height } = determineParameters(size, z, metaX, metaY);
			const metaUrl = `http://tileserver-gl:8080/styles/${id}/static/${lon},${lat},${zoom}/${width}x${height}.${format}`;
			
			const lockKey = `lock:${metaUrl}`;
			const channel = `metatile:${metaUrl}`;
			const lockAcquired = await client.set(lockKey, 'locked', 'NX', 'EX', 30);

			if (lockAcquired) {
				console.log('Aquired lock...');
				const tileImage = await fetchMetaTile(metaUrl, metaX, metaY, gridSize, origTileX, origTileY);
				client.del(lockKey);
				client.publish(channel, 'READY');
				console.log('raster complete -> notification sent');
				res.set('Content-Type', `image/${format}`);
				res.send(tileImage);
				return ;
			} 

			await new Promise((resolve, reject) => {
				subClient.subscribe(channel, (error) => {
					if (error) reject(error);
				});
	
				subClient.on('message', async (channel, message) => {
					if (message === 'READY' && channel === `metatile:${metaUrl}`) {
						const tileImage = await client.getBuffer(tileCacheKey);
						if (!tileImage) {
							reject(new Error('Image not found in cache after READY message'));
						} else {
							res.set('Content-Type', `image/${format}`);
							res.send(tileImage);
						}
						subClient.unsubscribe(channel);
						resolve();
					}
				});
			});
		}

	} catch (error) {
		console.error(error.message);
		res.status(500).send('Error fetching image: ' + error.message);
		if (subClient.isSubscribed) {
			subClient.unsubscribe();
		}
	}
});

app.get("*", (req, res) => {
	console.log("\n404 REQUEST:", req.headers);
	console.log("url:",req.url, "\n\n");
});

app.listen(port, () => {
	console.log(`Server is running on http://localhost:${port}`);
});